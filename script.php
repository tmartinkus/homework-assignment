<?php

use martinkus\CommissionFeeCalc\Services\CommissionFeeCalculator;

require_once __DIR__ . '/vendor/autoload.php';
$calc = new CommissionFeeCalculator();

if (isset($argv[1])) {
    $calc->print($argv[1]);
} else {
    print_r('No input file specified.');
}