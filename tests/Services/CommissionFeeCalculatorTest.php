<?php

declare(strict_types=1);

namespace martinkus\CommissionFeeCalc\Tests\Services;

use martinkus\CommissionFeeCalc\Services\CommissionFeeCalculator;
use PHPUnit\Framework\TestCase;

class CommissionFeeCalculatorTest extends TestCase
{
    /**
     * @var CommissionFeeCalculator
     */
    private $calc;

    public function setUp()
    {
        $this->calc = new CommissionFeeCalculator();
    }

    /**
     * @param string $filepath
     * @param string $rightOperand
     * @param string $expectation
     *
     * @dataProvider dataProviderForCalcTesting
     */
    public function testCalc(string $filepath, array $result)
    {
        $this->assertEquals(
            $result,
            $this->calc->calculate($filepath)
        );
    }

    public function dataProviderForCalcTesting(): array
    {
        return [
            'Data provided with the task' => ['input.csv', ['0.60', '3.00', '0.00', '0.06', '1.50', '0', '0.70', '0.30', '0.30', '3.00', '0.00', '0.00', '8612']],
        ];
    }
}
