<?php

declare(strict_types=1);

namespace martinkus\CommissionFeeCalc\Services;

use DateTime;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class CommissionFeeCalculator
{
    private const API_ROUTE = '//api.exchangeratesapi.io/v1/latest?access_key=b36f43fc67e70eab79f09d897c1e2218';
    private const WITHDRAW_LIMIT = 1000;
    private const FREE_WITHDRAW_OPERATION_LIMIT_PER_WEEK = 3;
    private const WITHDRAW_PRIVATE_FEE = 0.003;
    private const WITHDRAW_BUSINESS_FEE = 0.005;
    private const DEPOSIT_FEE = 0.0003;

    private $operations = [];
    private $userWithdrawals = []; // Used for private withdrawals, operations are grouped by user id and then by the week number
    private $rates = [];

    public function calculate(string $filepath): array
    {
        $this->operations = $this->parseFile($filepath);

        $this->calculateAmountsInEur();
        $this->parseWithdrawals();
        $this->calculateFees();

        $fees = [];
        foreach ($this->operations as $operation) {
            $fees[] = $operation['fee'];
        }

        return $fees;
    }

    public function print(string $filepath): void
    {
        foreach ($this->calculate($filepath) as $fee) {
            print_r($fee.PHP_EOL);
        }
    }

    private function parseFile(string $filepath): array
    {
        $operations = [];
        $file = fopen($filepath, 'r');

        if ($file) {
            while (($line = fgets($file)) !== false) {
                $operation = explode(',', $line);

                try {
                    $dt = (new DateTime($operation[0]));
                    $week = $dt->format('Y-W');
                    // year + week number for weeks that start in one year and end in another
                    if ($dt->format('W') === '01' && $dt->format('m') === '12') {
                        $week = (int) $dt->format('Y') + 1 .'-'.$dt->format('W');
                    }
                } catch (Exception $e) {
                    print_r('Failed to parse date '.$operation[0].'.'.PHP_EOL);
                    exit();
                }

                $operations[] = [
                    'date' => $operation[0],
                    'user_id' => intval($operation[1]),
                    'user_type' => $operation[2],
                    'type' => $operation[3],
                    'amount' => floatval($operation[4]),
                    'currency' => strlen($operation[5]) !== 3 ? substr($operation[5], 0, 3) : $operation[5],
                    'week' => $week,
                    'year' => explode('-', $operation[0])[0],
                ];
            }

            fclose($file);
        } else {
            print_r('File not found.');
        }

        return $operations;
    }

    private function calculateAmountsInEur(): void
    {
        $currencies = [];
        $rates = [];

        foreach ($this->operations as $operation) {
            if (!in_array($operation['currency'], $currencies, true)) {
                $currencies[] = $operation['currency'];
            }
        }

        if (!empty($currencies)) {
            $client = new Client();

            try {
                $res = $client->request('GET', self::API_ROUTE.'&base=EUR&symbols='.implode(',', $currencies));
                $rates = json_decode($res->getBody()->getContents(), true)['rates'];
            } catch (GuzzleException $e) {
                print_r('Failed to get conversion rates for '.implode(', ', $currencies).'.'.PHP_EOL.$e->getMessage());
                exit;
            }
        }

        foreach ($this->operations as $key => $operation) {
            if ($operation['currency'] !== 'EUR') {
                $this->operations[$key]['amount_in_eur'] = $operation['amount'] / $rates[$operation['currency']];
            } else {
                $this->operations[$key]['amount_in_eur'] = $operation['amount'];
            }
        }

        $this->rates = $rates;
    }

    private function parseWithdrawals(): void
    {
        foreach ($this->operations as $operation) {
            if ($operation['type'] === 'withdraw') {
                if (!isset($this->userWithdrawals[$operation['user_id']])) {
                    $this->userWithdrawals[$operation['user_id']] = [];
                }

                if (!isset($this->userWithdrawals[$operation['user_id']][$operation['week']])) {
                    $this->userWithdrawals[$operation['user_id']][$operation['week']] = [
                        'operations' => [],
                        'total' => 0,
                    ];
                }

                $this->userWithdrawals[$operation['user_id']][$operation['week']]['operations'][] = $operation;
                $this->userWithdrawals[$operation['user_id']][$operation['week']]['total'] += $operation['amount_in_eur'];
            }
        }
    }

    private function calculateFees(): void
    {
        foreach ($this->operations as $key => $operation) {
            $this->operations[$key]['fee'] = $this->calculateDecimalCeiling(
                $operation['type'] === 'deposit'
                ? $this->calculateDepositFee($operation)
                : $this->calculateWithdrawFee($operation),
                $operation['currency']
            );
        }
    }

    private function calculateDepositFee(array $operation): float
    {
        return self::DEPOSIT_FEE * $operation['amount'];
    }

    private function calculateWithdrawFee(array $operation): float
    {
        if ($operation['user_type'] === 'business') {
            return $operation['amount'] * self::WITHDRAW_BUSINESS_FEE;
        }

        $week = $this->userWithdrawals[$operation['user_id']][$operation['week']];

        if ($week['total'] > self::WITHDRAW_LIMIT) {
            $total = 0;

            foreach ($week['operations'] as $o) {
                if ($o === $operation) {
                    if ($total > self::WITHDRAW_LIMIT) {
                        return $operation['amount'] * self::WITHDRAW_PRIVATE_FEE;
                    }

                    if ($total + $o['amount_in_eur'] > self::WITHDRAW_LIMIT) {
                        return ($total + $o['amount_in_eur'] - self::WITHDRAW_LIMIT) * $this->rates[$o['currency']] * self::WITHDRAW_PRIVATE_FEE;
                    }
                }

                $total += $o['amount_in_eur'];
            }
        } else {
            if (sizeof($week['operations']) > self::FREE_WITHDRAW_OPERATION_LIMIT_PER_WEEK) {
                foreach ($week['operations'] as $key => $o) {
                    if ($key >= self::FREE_WITHDRAW_OPERATION_LIMIT_PER_WEEK) {
                        if ($o === $operation) {
                            return $operation['amount'] * self::WITHDRAW_PRIVATE_FEE;
                        }
                    }
                }
            }
        }

        return 0;
    }

    private function calculateDecimalCeiling(float $number, string $currency): string
    {
        $decimals = 2;
        $exceptions = include __DIR__.'/../currency-decimal-exceptions.php';

        if (key_exists($currency, $exceptions)) {
            $decimals = $exceptions[$currency];
        }

        return number_format(ceil($number / pow(10, -$decimals)) * pow(10, -$decimals), $decimals, '.', '');
    }
}
