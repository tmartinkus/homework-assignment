<?php

declare(strict_types=1);

return [
    'BHD' => 3,
    'CVE' => 0,
    'DJF' => 0,
    'GNF' => 0,
    'IDR' => 0,
    'IQD' => 3,
    'JOD' => 3,
    'JPY' => 0,
    'KMF' => 0,
    'KRW' => 0,
    'KWD' => 3,
    'LYD' => 3,
    'OMR' => 3,
    'PYG' => 0,
    'RWF' => 0,
    'TND' => 3,
    'UGX' => 0,
    'VND' => 0,
    'VUV' => 0,
    'XAF' => 0,
    'XOF' => 0,
    'XPF' => 0,
];
