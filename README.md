# homework assignment by martinkus

Install required packages:
```shell
composer install
```

Run the script:
```shell
php script.php input.csv
```

Run tests:
```shell
composer test
```